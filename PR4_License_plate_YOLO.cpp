#include <iostream>
#include <opencv2/opencv.hpp>


using namespace std;
using namespace cv;
using namespace dnn;

// Get the names of the output layers
vector<String> getOutputsNames(const Net& net)
{
    static vector<String> names;
    if (names.empty())
    {
        //Get the indices of the output layers, i.e. the layers with unconnected outputs
        vector<int> outLayers = net.getUnconnectedOutLayers();
         
        //get the names of all the layers in the network
        vector<String> layersNames = net.getLayerNames();
         
        // Get the names of the output layers in names
        names.resize(outLayers.size());
        for (size_t i = 0; i < outLayers.size(); ++i)
        names[i] = layersNames[outLayers[i] - 1];
    }
    return names;
}

/////////////////////Importing images///////////////////////
int main(void)
{

    /*give the configuration and weight files for the model*/
    string PathYOLO_cfg = "/home/krister/Downloads/yolov3 model-20240409T105942Z-001/yolov3 model/cfg/darknet-yolov3.cfg";
    string PathYOLO_weight = "/home/krister/Downloads/yolov3 model-20240409T105942Z-001/yolov3 model/weights/model.weights";
    /*the test image path */
    string path = "./images/test5.jpg";
    Mat img = imread(path); // matrix data type of opencv
    if(img.empty())
    {
        cout << "The file "<<path<<" is empty" << endl;
        return -1;
    }
    /*Load network*/
    Net license;
    license = readNetFromDarknet(PathYOLO_cfg,PathYOLO_weight);
    license.setPreferableBackend(DNN_BACKEND_OPENCV);
    license.setPreferableTarget(DNN_TARGET_CPU);

    vector<String> layerNames = license.getLayerNames();
    vector<int> outLayers = license.getUnconnectedOutLayers();
    Mat blob;
    blobFromImage(img,blob,1/255.0,Size(416,416),Scalar(255,0,255),true,false);
    /*Sets the input to the network*/
    license.setInput(blob);
    vector<Mat> outs;
    license.forward(outs, layerNames);
    vector<Rect> plates;
    // Post-process the detections
    for (auto& out : outs) {
        float* data = (float*)out.data;
        for (int j = 0; j < out.rows; ++j, data += out.cols) {
            Mat scores = out.row(j).colRange(5, out.cols);
            Point classIdPoint;
            double confidence;
            minMaxLoc(scores, 0, &confidence, 0, &classIdPoint);

            if (confidence > 0.2) {
                int centerX = (int)(data[0] * img.cols);
                int centerY = (int)(data[1] * img.rows);
                int width = (int)(data[2] * img.cols);
                int height = (int)(data[3] * img.rows);
                int left = centerX - width / 2;
                int top = centerY - height / 2;
                plates.push_back(Rect(left, top, width, height));
           }
        }
    }
    // Draw bounding boxes and save cropped images
    for (size_t i = 0; i < plates.size(); i++) {
        //Mat imgCrop = img(plates[i]);
        //imwrite("/plates/" + to_string(i) + ".png", imgCrop);
        rectangle(img, plates[i], Scalar(255, 0, 255), 3);
    }
    imshow("img",img);
    waitKey(0);/*wait for Infinity*/ 
    return 0;
}
